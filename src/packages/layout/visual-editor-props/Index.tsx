import { defineComponent, reactive, watch, PropType } from "vue";
import './Index.scss';
// 类型声明
import { VisualEditorModelValue } from '../../Index.d';
import { VisualEditorBlockData } from '../../components/visual-editor-block/Index.d';
import { VisualEditorConfig } from '../visual-editor-menu/Index.d';
import { VisualEditorProps } from './Index.d';
// 工具函数
import { VisualEditorPropsType } from './Index.utils';
// 组件插件
import TablePropEditor from '../../components/table-prop-editor/TablePropEditor';
import { ElForm, ElFormItem, ElInputNumber, ElButton, ElInput, ElColorPicker, ElSelect, ElOption } from 'element-plus';
import deepcopy from 'deepcopy';

const VisualEditorProps = defineComponent({
    props: {
        block: { // 当前选中的block元素，包含位置、属性、model等
            type: Object as PropType<VisualEditorBlockData>
        },
        config: {
            type: Object as PropType<VisualEditorConfig>,
            required: true
        },
        dataModel: {
            type: Object as PropType<{ value: VisualEditorModelValue }>,
            required: true
        }
    },
    emits: [
        'updateBlock',
        'updateDataModel'
    ],
    setup(props, ctx) {
        // 响应式数据
        const state = reactive({
            editData: '' as any, // 编辑数据
        });

        // 观察当前操作的block
        watch(() => props.block, (val) => {
            // 不存在，则编辑容器
            if (!val) {
                state.editData = deepcopy(props.dataModel.value.container); // {width: 100, height: 100}
                // 存在，则编辑当前block
            } else {
                state.editData = deepcopy(val || {});
            }
        }, { immediate: true });

        // 应用、重置功能按钮
        const methods = {
            apply: () => {
                // 编辑容器属性
                if (!props.block) {
                    ctx.emit('updateDataModel', {
                        ...props.dataModel.value,
                        container: state.editData
                    })
                    // 编辑block的属性
                } else {
                    const newBlock = state.editData;
                    ctx.emit('updateBlock', newBlock, props.block)
                }
            },
            reset: () => {
                if (!props.block) {
                    state.editData = deepcopy(props.dataModel.value.container);
                } else {
                    state.editData = deepcopy(props.block || {})
                }
            }
        };

        // 根据枚举，不同的类型渲染不同的组件
        const renderEditor = (propName: string, propConfig: VisualEditorProps) => {
            return {
                [VisualEditorPropsType.input]: () => (<ElInput v-model={state.editData.props[propName]} />),
                [VisualEditorPropsType.color]: () => (<ElColorPicker v-model={state.editData.props[propName]} />),
                [VisualEditorPropsType.select]: () => (<ElSelect v-model={state.editData.props[propName]}>
                    {(() => {
                        return propConfig.options!.map(opt => {
                            return <ElOption label={opt.label} value={opt.val} />
                        })
                    })()}
                </ElSelect>),
                [VisualEditorPropsType.table]: () => (
                    <TablePropEditor
                        v-model={state.editData.props[propName]} // 当前操作block的属性名， 如：type、size、color
                        propConfig={propConfig} // 当前操作block的属性值
                    />
                ),
            }[propConfig.type]()
        };

        return () => {
            let content: JSX.Element[] = [];
            // 没有选中任何block
            if (!props.block) {
                content.push(<>
                    <ElFormItem label="容器宽度">
                        <ElInputNumber v-model={state.editData.width} {...{ step: 100 }} />
                    </ElFormItem>
                    <ElFormItem label="容器高度">
                        <ElInputNumber v-model={state.editData.height} {...{ step: 100 }} />
                    </ElFormItem>
                </>)
                // 选中block，展示对应的操作功能组件
            } else {
                const { componentKey } = props.block; // 当前选中block的组件类型: 如：button, input, text, select 
                const component = props.config.componentMap[componentKey]; // {key: 'button', label: '按钮'，props: {}, preview: fn, render: fn}

                // 组件标识， slotName
                content.push(
                    <ElFormItem label="组件标识">
                        <ElInput v-model={state.editData.slotName} />
                    </ElFormItem>
                );

                if (!!component) {
                    // 组件绑定属性
                    if (!!component.props) {
                        content.push(<>
                            {
                                Object.entries(component.props || {}).map(([propName, propConfig]) => ( // props: {text:"xx", type:"primary", size:"24px", color:"red", options:[options:[], showKey: 'tag']}
                                    <ElFormItem label={propConfig.label} key={propName}>
                                        {renderEditor(propName, propConfig)}
                                    </ElFormItem>
                                ))
                            }
                        </>)
                    }
                    // 组件绑定字段
                    if (!!component.model) {
                        content.push(<>
                            {
                                Object.entries(component.model || {}).map(([moduleName, label]) => (
                                    <ElFormItem label={label}>
                                        <ElInput v-model={state.editData.model[moduleName]} />
                                    </ElFormItem>
                                ))
                            }
                        </>)
                    }
                }
            }

            return (
                <div class="visual-editor-props">
                    <h3>组件属性</h3>
                    <ElForm labelPosition="top">
                        {content}
                        <ElFormItem>
                            <ElButton {...{ onClick: methods.reset } as any}>重置</ElButton>
                            <ElButton type="primary" {...{ onClick: methods.apply } as any}>应用</ElButton>
                        </ElFormItem>
                    </ElForm>
                </div>
            )
        }
    }
})

export default VisualEditorProps;
